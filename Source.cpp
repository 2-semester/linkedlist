#include "LinkedList.h"
#include "LinkedList.cpp"
#include <iostream>

using namespace std;

int main() {
    LinkedList list;
    cout << "Initial list: " << list;

    list.addFirst(10);
    list.addFirst(20);
    list.addFirst(30);
    cout << "After adding 30, 20, 10 to the beginning: " << list;

    list.addLast(40);
    list.addLast(50);
    cout << "After adding 40, 50 to the end: " << list;

    list.addBeforeItem(20, 25);
    cout << "After adding 25 before 20: " << list;

    list.addAfterItem(40, 45);
    cout << "After adding 45 after 40: " << list;

    cout << "Is the list empty? " << (list.isEmpty() ? "Yes" : "No") << endl;

    list.clear();
    cout << "After clearing the list: " << list;
}
