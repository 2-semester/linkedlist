#include "LinkedList.h"
#include <iostream>

LinkedList::LinkedList() : head(nullptr), tail(nullptr)
{
}

LinkedList::LinkedList(int* values, int n) : head(nullptr), tail(nullptr)
{
    for (int i = n - 1; i >= 0; i--)
    {
        addFirst(values[i]);
    }
}

LinkedList::~LinkedList()
{
    clear();
    cout << *this;
}

bool LinkedList::isEmpty()
{
    return !head;
}

void LinkedList::addFirst(int value)
{
    Node* node = new Node(value);

    if (isEmpty())
    {
        head = tail = node;
    }
    else
    {
        node->next = head;
        head->prev = node;
        head = node;
    }
}

void LinkedList::addLast(int value)
{
    Node* node = new Node(value);

    if (isEmpty())
    {
        head = tail = node;
    }
    else
    {
        tail->next = node;
        node->prev = tail;
        tail = node;
    }
}

void LinkedList::addBeforeItem(int tag, int value)
{
    if (isEmpty())
    {
        return;
    }

    Node* current = head;

    while (current)
    {
        if (current->item == tag)
        {
            if (current == head)
            {
                addFirst(value);
            }
            else
            {
                Node* node = new Node(value);
                node->prev = current->prev;
                node->next = current;
                current->prev->next = node;
                current->prev = node;
            }
            return;
        }
        current = current->next;
    }
}

void LinkedList::addAfterItem(int tag, int value)
{
    if (isEmpty())
    {
        return;
    }

    Node* current = head;

    while (current)
    {
        if (current->item == tag)
        {
            Node* node = new Node(value);
            node->next = current->next;
            node->prev = current;
            if (current->next)
            {
                current->next->prev = node;
            }
            else
            {
                tail = node;
            }
            current->next = node;
            return;
        }
        current = current->next;
    }
}

void LinkedList::clear()
{
    Node* current = head;
    while (current)
    {
        Node* next = current->next;
        delete current;
        current = next;
    }
    head = tail = nullptr;
}

ostream& operator<<(ostream& stream, const LinkedList& list)
{
    LinkedList::Node* current = list.head;

    if (current == nullptr)
    {
        stream << "List is empty." << endl;
        return stream;
    }

    while (current)
    {
        stream << *current;
        current = current->next;
    }

    stream << "nullptr" << endl;

    return stream;
}
