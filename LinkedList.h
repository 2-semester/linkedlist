#pragma once
#include <ostream>

using namespace std;

class LinkedList
{
public:
    LinkedList();
    LinkedList(int*, int);
    ~LinkedList();
    bool isEmpty();
    void addFirst(int);
    void addLast(int);
    void addBeforeItem(int, int);
    void addAfterItem(int, int);
    void clear();
    friend ostream& operator<<(ostream& stream, const LinkedList& list);
private:
    struct Node
    {
        Node() : item(0), prev(nullptr), next(nullptr) {}
        Node(int value) : item(value), prev(nullptr), next(nullptr) {}
        friend ostream& operator<<(ostream& stream, const Node& node)
        {
            stream << node.item << " <-> ";
            return stream;
        }
        int item;
        Node* prev;
        Node* next;
    };

    Node* head;
    Node* tail;
};
